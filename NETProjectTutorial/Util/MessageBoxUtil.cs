﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial.Util
{
    class MessageBoxUtil
    {
        public static void InformationMessage(Form parent, string message)
        {
            MessageBox.Show(parent, message, "Mensaje de Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void ErrorMessage(Form parent, string message)
        {
            MessageBox.Show(parent, message, "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
