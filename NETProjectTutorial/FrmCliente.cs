﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drClientes;

        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }
        public DataTable TblClientes { set { tblClientes = value; } }
        public DataSet DsProductos { set { dsClientes = value; } }

        public DataRow DrClientes
        {
            set
            {
                drClientes = value;
                txtName.Text = drClientes["Nombres"].ToString();
                txtSurname.Text = drClientes["Apellidos"].ToString();
                txtCard.Text = drClientes["Cedula"].ToString();
                txtTelephone.Text = drClientes["Telefono"].ToString();
            }

        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string names, surname, card, telephone;

            names = txtName.Text;
            surname = txtSurname.Text;
            card = txtCard.Text;
            telephone = txtTelephone.Text;

            if (drClientes != null)
            {
                DataRow drNew = tblClientes.NewRow();

                int index = tblClientes.Rows.IndexOf(drClientes);
                drNew["Id"] = drClientes["Id"];
                drNew["Nombres"] = names;
                drNew["Apellidos"] = surname;
                drNew["Card"] = card;
                drNew["Telephone"] = telephone;


                tblClientes.Rows.RemoveAt(index);
                tblClientes.Rows.InsertAt(drNew, index);

            }
            else
            {
                tblClientes.Rows.Add(tblClientes.Rows.Count + 1, names, surname, 
                    card, telephone);
            }

            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = dsClientes;
            bsClientes.DataMember = dsClientes.Tables["Cliente"].TableName;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
