﻿using Nancy.Json;
using NETProjectTutorial.entities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> LstClientes = new List<Cliente>();

        public static List<Cliente> GetLstCliente()
        {
            return LstClientes;
        }

        public static void Populate()
        {

            Cliente[] clientes =
            {
                new Cliente(1, "Bryan", "Hernández", "88888888", "001-031000-1087V")
            };

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LstClientes = serializer.Deserialize<List<Cliente>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.ClienteData));

        }
    }
}
