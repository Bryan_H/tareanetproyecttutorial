﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Names
    {
        String nameEm;
        String nameCli;

        public string NameEmp{ get => nameEm; set => nameEm = value; }
        public string NameCli { get => nameCli; set => nameCli = value; }

    }
}
