﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ProductoFactura
    {
        private string sku;
        private string nombre;
        private int cantidad;
        private double precio;

        public ProductoFactura(string sku, string nombre, int cantidad, double precio)
        {
            this.sku = sku;
            this.nombre = nombre;
            this.cantidad = cantidad;
            this.precio = precio;
        }

        public string Sku { get => sku; set => sku = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double Precio { get => precio; set => precio = value; }
    }
}
